# Description
This is a NodeJS express application which is using Mongo Atlas as a cloud database.

You can list, add, update or remove products on this example.
# Polymer Installation
```sh
$ npm install -g polymer-cli
```
For more information please visit:

https://polymer-library.polymer-project.org/3.0/docs/install-3-0

# Demo Usage
```sh
$ git clone https://kemal-muderrisoglu@bitbucket.org/kemal-muderrisoglu/polymer-demo.git
$ cd polymer-demo
$ npm install
$ npm start
```
Server is started on http://localhost:3000 :)
