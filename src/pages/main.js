import {LitElement, html} from 'lit-element';
import {render} from 'lit-html';
import '@vaadin/vaadin-crud/vaadin-crud.js';

class MainPage extends LitElement {
    static get properties() {
        return {
            products: Array,
            editing: Boolean,
        }
    }

    firstUpdated() {
        super.firstUpdated();

        fetch(`products/all`,
            {
                method: 'GET',
                headers: {"Content-Type": "application/json"},
            })
            .then(r => r.json())
            .then(data => {
                if (data.body.length > 0)
                    this.products = data.body;
            });
        this.editing = false;
    }

    onDelete(e) {
        fetch(`/products/${e.detail.item._id}`,
            {
                method: 'DELETE',
                headers: {"Content-Type": "application/json"}
            })
            .then(r => r.json());
        this.editing = false;
    }

    onSave(e) {
        if (!this.editing) {
            fetch(`products/new`,
                {
                    method: 'POST',
                    headers: {"Content-Type": "application/json"},
                    body: JSON.stringify(e.detail.item)
                })
                .then(r => r.json());
        } else {
            fetch(`/products/${e.detail.item._id}`,
                {
                    method: 'PUT',
                    headers: {"Content-Type": "application/json"}
                })
                .then(r => r.json());
        }
        this.editing = false;
    }

    onEdit() {
        this.editing = true
    }

    onCancel() {
        this.editing = false
    }

    render() {
        return html`
        <vaadin-crud 
            .items="${this.products}" 
            include="productName,category,imageUrl,price"
            @delete=${this.onDelete}"
            @save=${this.onSave}"
            @edit=${this.onEdit}"
            @cancel=${this.onCancel}"
        >
            <vaadin-form-layout slot="form">
                <vaadin-text-field path="productName" label="Product Name" required></vaadin-text-field>
                <vaadin-text-field path="category" label="Category" required></vaadin-text-field>
                <vaadin-text-field path="imageUrl" label="Image URL" required pattern="(https?:\\/\\/.*\\.(gif|jpe?g|bmp|png))" error-message="only (gif|jpg|jpeg|bmp|png) images are accepted"></vaadin-text-field>
                <vaadin-text-field path="price" label="Price" required pattern="[0-9]+((.|,)[0-9]+)?" error-message="price should be a numeric value"></vaadin-text-field>
            </vaadin-form-layout>
            
            <vaadin-grid slot="grid">
                <vaadin-grid-column>
                      <template class="header">Product Name</template>
                      <template>[[item.productName]]</template>
                </vaadin-grid-column>
                <vaadin-grid-column>
                      <template class="header">Category</template>
                      <template>[[item.category]]</template>
                </vaadin-grid-column>
                <vaadin-grid-column>
                    <template class="header">Image URL</template>
                    <template><img src="[[item.imageUrl]]" style="height: 50px; border-radius: 50%;"></template>
                 </vaadin-grid-column>
                <vaadin-grid-column>
                      <template class="header">Price</template>
                      <template>[[item.price]]</template>
                </vaadin-grid-column>
                <vaadin-crud-edit-column></vaadin-crud-edit-column>
             </vaadin-grid>
        </vaadin-crud>
    `;
    }
}

window.customElements.define('main-page', MainPage);
