const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({
    productName: String,
    category: String,
    imageUrl: String,
    price: Number
});

module.exports = mongoose.model('Product', ProductSchema);
