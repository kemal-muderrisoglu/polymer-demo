const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const config = require('./config/default');
const productsRouter = require('./routes/products');
let app = express();

mongoose.Promise = global.Promise;

mongoose.connect(config.db.url, {useNewUrlParser: true})
    .then(() => console.log('Connected to MongoDB.'))
    .catch((err) => console.error(err));

app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../build/dist')));

app.use('/products', productsRouter);

module.exports = app;
