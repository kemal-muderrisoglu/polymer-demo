const router = require('express').Router();
const Product = require('../models/Product.js');

/* GET ALL PRODUCTS */
router.get('/all', function (req, res, next) {
    Product.find()
        .then(product => res.json({
            statusCode: 200,
            body: product
        }))
        .catch(err => res.json({
            statusCode: err.statusCode || 500,
            headers: {'Content-Type': 'application/json'},
            message: 'Could not fetch the product.'
        }))
});

/* CREATE PRODUCT */
router.post('/new', function (req, res, next) {
    Product.create(req.body)
        .then(product => res.json({
            statusCode: 200,
            body: product
        }))
        .catch(err => res.json({
            statusCode: err.statusCode || 500,
            headers: {'Content-Type': 'application/json'},
            message: 'Could not fetch the product.'
        }))
});

/* UPDATE PRODUCT */
router.put('/:productId', function (req, res, next) {
    Product.findByIdAndUpdate(req.params.productId, req.body)
        .then(product => res.json({
            statusCode: 200,
            body: product
        }))
        .catch(err => res.json({
            statusCode: err.statusCode || 500,
            headers: {'Content-Type': 'application/json'},
            message: 'Could not fetch the product.'
        }));
});

/* DELETE PRODUCT */
router.delete('/:productId', function (req, res, next) {
    Product.findByIdAndRemove(req.params.productId)
        .then(product => res.json({
            statusCode: 200,
            message: 'Removed product with id:' + product._id,
            body: product
        }))
        .catch(err => res.json({
            statusCode: err.statusCode || 500,
            headers: {'Content-Type': 'application/json'},
            message: 'Could not fetch the product.'
        }));
});

module.exports = router;
